﻿namespace Clay.Domain.Coomon
{
    public static class Constants
    {
        public static readonly string Issuer = "https://www.my-clay.com";

        public static readonly string EmployeeTagName = "EmployeeTag";
        public static readonly string DepartmentName = "Department";
    }
}
