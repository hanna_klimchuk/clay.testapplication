﻿using AutoMapper;
using Clay.Domain.Entities;
using Dto = Clay.Domain.ApiModels;

namespace Clay.Domain.Mappers
{
    public class UserMappingProfile : Profile
    {
        public UserMappingProfile()
        {
            CreateMap<Dto.Department, Department>().ReverseMap();
        }
    }
}
