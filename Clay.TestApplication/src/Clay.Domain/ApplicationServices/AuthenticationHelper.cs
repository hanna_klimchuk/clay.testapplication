﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using Clay.Domain.ApiModels;
using Clay.Domain.ApplicationServices.Interfaces;
using Clay.Domain.Coomon;
using Clay.Domain.Entities;
using Microsoft.AspNetCore.Identity;
using Department = Clay.Domain.Entities.Department;

namespace Clay.Domain.ApplicationServices
{
    public class AuthenticationHelper : IAuthenticationHelper
    {
        private readonly UserManager<ClayUser> _userManager;
        private readonly IMapper _mapper;

        public AuthenticationHelper(
            UserManager<ClayUser> userManager,
            IMapper mapper)
        {
            _userManager = userManager;
            _mapper = mapper;
        }

        public async Task<bool> ValidateUserAsync(LoginRequest request)
        {
            var user = await _userManager.FindByEmailAsync(request.Email);
            if (user == null)
            {
                return false;
            }

            var passwordsMatch = await _userManager.CheckPasswordAsync(user, request.Password);

            return passwordsMatch;
        }

        public async Task<ClaimsPrincipal> GetClaimsPrincipal(string email)
        {
            var user = await _userManager.FindByEmailAsync(email);
            var claims = await _userManager.GetClaimsAsync(user);

            return CreateClaimsPrincipal(claims);
        }

        public async Task<RegistrationResponse> CreateUserAsync(RegistrationRequest request)
        {
            var user = CreateUserFromRequest(request);
            var claims = CreateClaimsForUser(user);

            var identityResult = await _userManager.CreateAsync(user, request.Password);
            if (identityResult.Succeeded)
            {
                await _userManager.AddClaimsAsync(user, claims);

                return new RegistrationResponse
                {
                    Success = true,
                    UserPrincipal = CreateClaimsPrincipal(claims)
                };
            }

            return new RegistrationResponse
            {
                Success = false,
                Errors = identityResult.ToString()
            };
        }

        private static IEnumerable<Claim> CreateClaimsForUser(ClayUser user)
        {
            return new List<Claim>
            {
                new Claim(ClaimTypes.Email, user.Email, ClaimValueTypes.String, Constants.Issuer),
                new Claim("EmployeeTag", user.CustomTag.ToString(), ClaimValueTypes.String, Constants.Issuer),
                new Claim("Department", user.Department.ToString(), ClaimValueTypes.String, Constants.Issuer)
            };
        }

        private static ClaimsPrincipal CreateClaimsPrincipal(IEnumerable<Claim> claims)
        {
            var userIdentity = new ClaimsIdentity(claims, "Clay.TestApplication");

            return new ClaimsPrincipal(userIdentity);
        }

        private ClayUser CreateUserFromRequest(RegistrationRequest request)
        {
            var tag = Guid.NewGuid();
            return new ClayUser
            {
                UserName = request.Email,
                Email = request.Email,
                Department = _mapper.Map<Department>(request.Department),
                PhoneNumber = request.PhoneNumber,
                CustomTag = tag
            };
        }
    }
}
