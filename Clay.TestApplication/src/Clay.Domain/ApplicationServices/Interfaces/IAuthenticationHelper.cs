﻿using System.Security.Claims;
using System.Threading.Tasks;
using Clay.Domain.ApiModels;

namespace Clay.Domain.ApplicationServices.Interfaces
{
    public interface IAuthenticationHelper
    {
        Task<bool> ValidateUserAsync(LoginRequest request);
        Task<ClaimsPrincipal> GetClaimsPrincipal(string email);
        Task<RegistrationResponse> CreateUserAsync(RegistrationRequest request);
    }
}
