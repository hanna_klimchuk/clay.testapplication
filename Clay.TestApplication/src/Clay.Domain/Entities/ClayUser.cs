﻿using System;
using Microsoft.AspNetCore.Identity;

namespace Clay.Domain.Entities
{
    public class ClayUser : IdentityUser
    {
        public Guid CustomTag { get; set; }

        public Department Department { get; set; }
    }
}
