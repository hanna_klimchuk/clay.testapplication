﻿namespace Clay.Domain.Entities
{
    public enum Department
    {
        Marketing, 
        Finance, 
        OperationsManagement, 
        HumanResource,
        IT
    }
}