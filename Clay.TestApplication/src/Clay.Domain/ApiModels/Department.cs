﻿namespace Clay.Domain.ApiModels
{
    public enum Department
    {
        Marketing,
        Finance,
        OperationsManagement,
        HumanResource,
        IT
    }
}
