﻿using System.ComponentModel.DataAnnotations;

namespace Clay.Domain.ApiModels
{
    public class LoginRequest
    {
        [EmailAddress]
        [Required(ErrorMessage = "Email is required")]
        public string Email { get; set; }

        [StringLength(50, MinimumLength = 5)]
        [Required(ErrorMessage = "Password is required")]
        public string Password { get; set; }
    }
}
