﻿using System.Security.Claims;

namespace Clay.Domain.ApiModels
{
    public class RegistrationResponse
    {
        public bool Success { get; set; }

        public ClaimsPrincipal UserPrincipal { get; set; }

        public string Errors { get; set; }
    }
}
