﻿using System;
using System.Threading.Tasks;
using Clay.Domain.ApiModels;
using Clay.Domain.ApplicationServices.Interfaces;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Clay.WebService.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [AllowAnonymous]
    public class AccountController : Controller
    {
        private readonly IAuthenticationService _authenticationService;
        private readonly IAuthenticationHelper _authenticationHelper;
        private readonly ILogger<AccountController> _logger;

        public AccountController(
            IAuthenticationService authenticationService, 
            IAuthenticationHelper authenticationHelper, 
            ILogger<AccountController> logger)
        {
            _authenticationService = authenticationService;
            _authenticationHelper = authenticationHelper;
            _logger = logger;
        }

        [HttpGet("login")]
        public IActionResult Login()
        {
            return Ok("Redirected to login page!");
        }

        [HttpPost("login")]
        public async Task<IActionResult> LoginAsync([FromBody] LoginRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var validationResult = await _authenticationHelper.ValidateUserAsync(request);
            if (!validationResult)
            {
                return Unauthorized();
            }

            var userPrincipal = await _authenticationHelper.GetClaimsPrincipal(request.Email);

            await _authenticationService.SignInAsync(
                HttpContext,
                CookieAuthenticationDefaults.AuthenticationScheme,
                userPrincipal,
                new AuthenticationProperties
                {
                    ExpiresUtc = DateTime.UtcNow.AddMinutes(20),
                    IsPersistent = true,
                    AllowRefresh = false
                });

            _logger.LogInformation($"User {request.Email} logged in");

            return Ok();
        }

        [HttpPost("register")]
        public async Task<IActionResult> RegisterAsync([FromBody] RegistrationRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var registrationResult = await _authenticationHelper.CreateUserAsync(request);

            if (registrationResult.Success)
            {
                await _authenticationService.SignInAsync(
                    HttpContext,
                    CookieAuthenticationDefaults.AuthenticationScheme,
                    registrationResult.UserPrincipal,
                    new AuthenticationProperties
                    {
                        ExpiresUtc = DateTime.UtcNow.AddMinutes(20),
                        IsPersistent = true,
                        AllowRefresh = false
                    });

                _logger.LogInformation($"User {request.Email} account created.");

                return Ok();
            }

            return StatusCode(500, registrationResult.Errors);
        }
    
        [HttpGet("logout")]
        public async Task<IActionResult> LogOutAsync()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            //await _signInManager.SignOutAsync();
            return Ok();
        }
    }
}
