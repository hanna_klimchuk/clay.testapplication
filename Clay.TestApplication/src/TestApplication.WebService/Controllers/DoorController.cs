﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Clay.WebService.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class DoorController : Controller
    {
        [HttpGet("office/opening")]
        [Authorize(Policy = PolicyNames.OfficeEntry)]
        public ActionResult OpenOfficeDoor()
        {
            return Ok("Office door opened!");
        }

        [HttpGet("tunnel/opening")]
        [Authorize(Policy = PolicyNames.TunnelEntry)]
        public ActionResult OpenTunnelDoor()
        {
            return Ok("Tunnel door opened!");
        }
    }
}
