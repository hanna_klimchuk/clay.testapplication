﻿using AutoMapper;
using Clay.Domain.Mappers;
using Microsoft.Extensions.DependencyInjection;

namespace Clay.WebService.Configuration
{
    public static class ServicesConfiguration
    {
        public static IServiceCollection AddMapper(this IServiceCollection services)
        {
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new UserMappingProfile());
            });

            IMapper mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);

            return services;
        }
    }
}
