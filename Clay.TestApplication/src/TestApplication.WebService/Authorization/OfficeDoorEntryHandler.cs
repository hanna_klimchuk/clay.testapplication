﻿using System.Threading.Tasks;
using Clay.Domain.Coomon;
using Clay.WebService.Authorization.Requirements;
using Microsoft.AspNetCore.Authorization;

namespace Clay.WebService.Authorization
{
    public class OfficeDoorEntryHandler : AuthorizationHandler<OfficeDoorEntryRequirement>
    {
        protected override Task HandleRequirementAsync(
            AuthorizationHandlerContext context, 
            OfficeDoorEntryRequirement requirement)
        {
            if (!context.User.HasClaim(c => c.Type == Constants.EmployeeTagName &&
                                            c.Issuer == Constants.Issuer))
            {
                return Task.CompletedTask;
            }

            context.Succeed(requirement);

            return Task.CompletedTask;
        }
    }
}
