﻿using System.Threading.Tasks;
using Clay.Domain.ApiModels;
using Clay.Domain.Coomon;
using Clay.WebService.Authorization.Requirements;
using Microsoft.AspNetCore.Authorization;

namespace Clay.WebService.Authorization
{
    public class MarketingOnlyEntryHandler : AuthorizationHandler<TunnelDoorEntryRequirement>
    {
        protected override Task HandleRequirementAsync(
            AuthorizationHandlerContext context,
            TunnelDoorEntryRequirement requirement)
        {
            if (!context.User.HasClaim(c => c.Type == Constants.DepartmentName && 
                                            c.Value == Department.Marketing.ToString() &&
                                            c.Issuer == Constants.Issuer))
            {
                return Task.CompletedTask;
            }

            context.Succeed(requirement);

            return Task.CompletedTask;
        }
    }
}
