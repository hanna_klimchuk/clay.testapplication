using Clay.DataEFCore;
using Clay.Domain.ApplicationServices;
using Clay.Domain.ApplicationServices.Interfaces;
using Clay.Domain.Entities;
using Clay.WebService.Authorization;
using Clay.WebService.Authorization.Requirements;
using Clay.WebService.Configuration;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Clay.WebService
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public virtual void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers(config =>
            {
                var policy = new AuthorizationPolicyBuilder()
                    .RequireAuthenticatedUser()
                    .Build();
                config.Filters.Add(new AuthorizeFilter(policy));
            });

            services.AddSingleton<IAuthorizationHandler, OfficeDoorEntryHandler>();
            services.AddSingleton<IAuthorizationHandler, MarketingOnlyEntryHandler>();
            services.AddScoped<IAuthenticationHelper, AuthenticationHelper>();

            services.AddDbContext<ApplicationDbContext>(
                options =>
                    options.UseSqlServer(Configuration.GetConnectionString("ApplicationDatabase")))
                .AddDefaultIdentity<ClayUser>()
                .AddEntityFrameworkStores<ApplicationDbContext>();

            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                .AddCookie(CookieAuthenticationDefaults.AuthenticationScheme,
                    options =>
                    {
                        options.LoginPath = new PathString("/Account/Login");
                        options.LogoutPath = new PathString("/Account/Logout");
                    });

            services.AddAuthorization(options =>
            {
                options.AddPolicy(PolicyNames.OfficeEntry, 
                    policy => policy.Requirements.Add(new OfficeDoorEntryRequirement()));
                options.AddPolicy(PolicyNames.TunnelEntry, 
                    policy =>
                    {
                        policy.Requirements.Add(new OfficeDoorEntryRequirement());
                        policy.Requirements.Add(new TunnelDoorEntryRequirement());
                    });
            });

            services.AddMapper();
        }

        public virtual void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            using (var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetRequiredService<ApplicationDbContext>();
                context.Database.EnsureCreated();
                context.Database.Migrate();
            }

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "api/{controller=Door}/{action=Office}");
                endpoints.MapControllers();
            });
        }
    }
}
