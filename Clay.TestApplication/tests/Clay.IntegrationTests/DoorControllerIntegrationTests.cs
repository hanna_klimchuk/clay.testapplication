﻿using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Clay.WebService;
using Microsoft.AspNetCore.Mvc.Testing;
using Xunit;

namespace Clay.IntegrationTests
{
    public class DoorControllerIntegrationTests : IClassFixture<TestingWebAppFactory<Startup>>
    {
        private readonly HttpClient _client;

        public DoorControllerIntegrationTests(TestingWebAppFactory<Startup> factory)
        {
            _client = factory.CreateClient(
                new WebApplicationFactoryClientOptions
                {
                    AllowAutoRedirect = false
                });
        }

        [Fact]
        public async Task Get_OpenOfficeDoor_UnauthorizedRedirect()
        {
            // Arrange

            // Act
            var response = await _client.GetAsync("/api/door/office/opening");

            // Assert
            Assert.Equal(HttpStatusCode.Redirect, response.StatusCode);
            Assert.StartsWith("http://localhost/Account/Login", response.Headers.Location.OriginalString);
        }
    }


}
